import pytest
import weather


API_KEY = '00013963ec9ce43bde94fa3adc2776b9'
HELP_MESSAGE = 'Usage: python weather.py <city> <country>\n\nExample:\n./weather.py kostroma ru\n' \
               'or default location:\n./weather.py\n'
HEADER_MESSAGE = '\nWeather and forecasts in Moscow (RU)\n\n'
MOSCOW_COORDINATES = {'lat': 55.75, 'lon': 37.62}


def test_show_help(capsys):
    weather.show_help()
    captured = capsys.readouterr()
    assert captured.out == HELP_MESSAGE


def test_show_header(capsys):
    weather.show_header('Moscow', 'ru')
    captured = capsys.readouterr()
    assert captured.out == HEADER_MESSAGE


def test_connect():
    assert weather.connect(None) is None


def test_get_city_coords():
    assert weather.get_city_coords(owm=weather.connect(API_KEY), city='Moscow') == MOSCOW_COORDINATES


def test_execution():
    with pytest.raises(SystemExit):
        weather.main()

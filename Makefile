.DEFAULT_GOAL = run

.PHONY: deps
deps: requirements.txt
	@(virtualenv venv; \
	source venv/bin/activate; \
	pip install -r requirements.txt)

.PHONY: clean
clean:
	@find . -name '*.pyc' -exec rm --force {} +
	@find . -name '*.pyo' -exec rm --force {} +

.PHONY: clean-docs
clean-docs:
	@find . -name '*.html' -exec rm --force {} +

.PHONY: test
test: clean
	@(source venv/bin/activate; \
	flake8 --ignore E501,E722 *.py; \
	py.test --verbose --color=yes .)

.PHONY: docs
docs: clean-docs
	@venv/bin/python -m pydoc -w weather

.PHONY: run
run:
	@venv/bin/python weather.py

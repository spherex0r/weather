Simple demo application
---

Prepare environment:

`sudo yum install -y python-pip python-virtualenv` for RHEL or CentOS.

`sudo apt install virtualenv` for Debian or Ubuntu.

Start application:

`make` or `make help` to get help.

Start tests:

`make test`

Generate docs:

`make docs`

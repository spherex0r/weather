#!/usr/bin/env python
"""
Simple demo application
"""

import socket
import sys

import pyowm
import pyowm.exceptions as e

API_KEY = '00013963ec9ce43bde94fa3adc2776b9'  # open weaher map web api key
COUNTRY = 'RU'  # default country
CITY = 'Saint Petersburg'  # default city
REGION_LIMIT = 2  # city limit for a region


def show_help():
    """
    Shows help message

    :return: None
    """
    print('Usage: python weather.py <city> <country>')
    print('')
    print('Example:')
    print('./weather.py kostroma ru')
    print('or default location:')
    print('./weather.py')


def show_header(city, country):
    """
    Shows command line header message

    :param city: String
    :param country: String
    :return: None
    """
    print('')
    print('Weather and forecasts in {} ({})'.format(city.capitalize(), country.upper()))
    print('')


def connect(api_key):
    """
    Create Open Weather Map object with given Web API key

    :param api_key: String
    :return: pyowm.OWM
    """
    owm = pyowm.OWM(api_key)
    return owm


def get_city_coords(owm, city):
    """
    Get coordinates by given city name in a region

    :param owm: pyowm.OWM
    :param city: String
    :return: Dictionary
    """
    observation = owm.weather_at_place(city)
    w = observation.get_location()
    coords = dict()
    coords['lon'] = w.get_lon()
    coords['lat'] = w.get_lat()
    return coords


def city_weather(owm, city):
    """
    Get info about weather in a given city

    :param owm: pyowm.OWM
    :param city: String
    :return: pyowm.observation
    """
    observation = owm.weather_at_place(city)
    return observation.get_weather()


def region_weather(owm, coords, region_limit=REGION_LIMIT):
    """
    Get info about weather in a region around given city

    :param owm: pyowm.OWM
    :param coords: Dictionary
    :param region_limit: Integer
    :return: List of pyowm.observation
    """
    observation_list = owm.weather_around_coords(coords['lat'], coords['lon'], region_limit)
    observations = list()
    for o in observation_list:
        observations.append(o.get_weather())
    return observations


def weather_cast(observation, cast_type):
    """
    Shows weather cast for given observation

    :param observation: pyowm.observation
    :param cast_type: String
    :return: None
    """
    w = observation

    print('{}'.format(cast_type))
    print('---')

    print('Current state:\t{}'.format(w.get_detailed_status()))
    print('Clouds, %\t{}'.format(w.get_clouds()))
    print('Humidity, %\t{}'.format(w.get_humidity()))

    wind = w.get_wind(unit='meters_sec')
    print('Wind:')
    print('  speed, m/s\t{}'.format(wind['speed']))
    print('  direction, d\t{}'.format(wind['deg']))

    temp = w.get_temperature('celsius')
    print('Temperature:')
    print('  current, c\t{}'.format(temp['temp']))
    print('  min, c\t{}'.format(temp['temp_min']))
    print('  max, c\t{}'.format(temp['temp_max']))
    print('')


def main():
    """
    Main function (made for tests)

    :return: None
    """
    try:
        if len(sys.argv) > 3:
            show_help()
            sys.exit(1)
        elif sys.argv[1] and (sys.argv[1] == '--help' or sys.argv[1] == '-h'):
            show_help()
            exit()
        elif sys.argv[1] and sys.argv[2]:
            city = sys.argv[1]
            country = sys.argv[2]
    except IndexError:
        print('Used default location: {} ({})'.format(CITY, COUNTRY))
        city = CITY
        country = COUNTRY

    location = city + ',' + country

    try:
        show_header(city, country)
        owm = connect(api_key=API_KEY)
        region = get_city_coords(owm, city=location)
        weather_cast(city_weather(owm, city=location), 'City')
        for location in region_weather(owm, coords=region):
            weather_cast(location, 'Region')
    except e.unauthorized_error.UnauthorizedError:
        print('ERROR: Your API key have no sufficient capabilities or simply wrong')
        sys.exit(1)
    except e.not_found_error.NotFoundError:
        print('ERROR: Unable to find your location with given city or country')
    except e.parse_response_error.ParseResponseError:
        print('ERROR: Data from Open Weather Map response can''t be parsed')
        sys.exit(1)
    except e.api_call_error.APICallError:
        print('ERROR: Open Weather Map web API can''t be reached')
        sys.exit(1)
    except socket.error:
        print('ERROR: Connection refused. Check your network access')
        sys.exit(1)
    except:
        print "Unexpected error:", sys.exc_info()[0]
        raise
    quit()


if __name__ == '__main__':
    main()
